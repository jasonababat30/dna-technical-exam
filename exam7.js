function descendingSort(array){

	let sortedArr = array.sort();

	let reverseSort = [];

	let twoDigitArr = [];

	for(let i = 0; i < array.length; i++){
		if(sortedArr[i] > 9){
			twoDigitArr.push(sortedArr[i]);
		} else {
			reverseSort.unshift(sortedArr[i]);
		}
	}

	for (let i = 0; i < twoDigitArr.length; i++){
		reverseSort.unshift(twoDigitArr[i]);
	}

	return reverseSort;

}