function removeDuplicates(array){
	let  uniqueArr = [];
	let commonArr = [];

	for  (let i = 0; i < array.length; i++){
		if(uniqueArr.indexOf(array[i]) === -1){
			uniqueArr.push(array[i]);
		} else {
			commonArr.push(array[i]);
		}
	}

	return uniqueArr;
}