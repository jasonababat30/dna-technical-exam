function findMean(array){

	let totalOfArray = 0;

	for(let i = 0; i < array.length; i++){
		totalOfArray += array[i];
	}

	let meanOfArray = totalOfArray / array.length;

	return meanOfArray;

}


function findMedian(array){

	let medianOfArray = array.length / 2;

	return array[medianOfArray - 1];

}


function findMode(array){

	let uniqueArr = [];

	let commonArr = [];

	for(let i = 0; i < array.length; i++){
		if(array.indexOf(array[i]) === -1){
			uniqueArr.push(array[i]);
		} else {
			commonArr.push(array[i]);
		}
	}

	if(commonArr.length < 1){
		return 'no mode';
	}

}